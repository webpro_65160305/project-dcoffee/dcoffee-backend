import { Module } from '@nestjs/common';
import { TypesService } from './types.service';
import { TypesController } from './types.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Type } from './entities/type.entity';
import { JwtModule } from '@nestjs/jwt'; // Import JwtModule
import { JwtService } from '@nestjs/jwt'; // Import JwtService

@Module({
  imports: [
    TypeOrmModule.forFeature([Type]),
    JwtModule.register({
      secret: 'your_secret_key', // Provide your secret key here
      signOptions: { expiresIn: '1h' },
    }),
  ],
  controllers: [TypesController],
  providers: [TypesService, JwtService], // Ensure JwtService is provided
})
export class TypesModule {}
