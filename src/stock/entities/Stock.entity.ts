import { StockReceiptItems } from 'src/stock-receipt/entities/StockReceiptItem.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Stock {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  priceUnit: number;

  @Column()
  quantity: number;

  @Column()
  value: number;

  @Column({ default: 'nostock.jpg' })
  image: string;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @OneToMany(
    () => StockReceiptItems,
    (stockReceiptItems) => stockReceiptItems.stock,
  )
  stockItems: StockReceiptItems[];
}
