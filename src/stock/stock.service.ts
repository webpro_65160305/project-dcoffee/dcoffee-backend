import { Injectable } from '@nestjs/common';
import { CreateStockDto } from './dto/create-Stock.dto';
import { UpdateStockDto } from './dto/update-Stock.dto';
import { Stock } from './entities/Stock.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class StockService {
  constructor(
    @InjectRepository(Stock) private stockRepository: Repository<Stock>,
  ) {}
  create(createStockDto: CreateStockDto) {
    const stock = new Stock();
    stock.name = createStockDto.name;
    stock.priceUnit = createStockDto.priceUnit;
    stock.quantity = createStockDto.quantity;
    stock.value = createStockDto.quantity * createStockDto.priceUnit;
    if (createStockDto.image && createStockDto.image !== '') {
      stock.image = createStockDto.image;
    }
    return this.stockRepository.save(stock);
  }
  findAll() {
    return this.stockRepository.find();
  }
  findOne(id: number) {
    return this.stockRepository.findOneBy({ id: id });
  }

  async update(id: number, updateStockDto: UpdateStockDto) {
    const stock = await this.stockRepository.findOneOrFail({
      where: { id },
    });
    const quantity = stock.quantity;
    const priceUnit = stock.priceUnit;
    stock.name = updateStockDto.name;
    stock.quantity = updateStockDto.quantity;
    stock.priceUnit = updateStockDto.priceUnit;
    if (stock.quantity == undefined) {
      stock.value = updateStockDto.priceUnit * quantity;
    } else if (stock.priceUnit == undefined) {
      stock.value = priceUnit * updateStockDto.quantity;
    } else {
      stock.value = updateStockDto.priceUnit * updateStockDto.quantity;
    }

    await this.stockRepository.save(stock);
    const result = await this.stockRepository.findOne({
      where: { id },
    });
    return result;
  }

  async remove(id: number) {
    const deleteStock = await this.stockRepository.findOneOrFail({
      where: { id },
    });
    await this.stockRepository.remove(deleteStock);
    return deleteStock;
  }
}
