export class CreateStockDto {
  name: string;

  priceUnit: number;

  quantity: number;

  value: number;

  image: string;
}
