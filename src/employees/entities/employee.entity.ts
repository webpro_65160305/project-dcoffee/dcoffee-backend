import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity()
export class Employee {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  gender: string;

  @Column()
  tel: string;

  @Column()
  username: string;

  @Column()
  password: string;

  @Column()
  role: string;

  // @OneToOne(() => Role, { cascade: true, eager: true }) //onetoone
  // @JoinColumn()
  // role: Role;

  @Column()
  bankAccount: string;
}
