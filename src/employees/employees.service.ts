import { Injectable } from '@nestjs/common';
import { CreateEmployeeDto } from './dto/create-employee.dto';
import { UpdateEmployeeDto } from './dto/update-employee.dto';
import { Employee } from './entities/employee.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class EmployeesService {
  constructor(
    @InjectRepository(Employee)
    private employeesRepository: Repository<Employee>,
  ) {}
  create(createEmployeeDto: CreateEmployeeDto) {
    return this.employeesRepository.save(createEmployeeDto);
  }

  findAll() {
    return this.employeesRepository.find();
  }

  findOne(id: number) {
    return this.employeesRepository.findOneBy({ id });
  }

  async update(id: number, updateEmployeeDto: UpdateEmployeeDto) {
    await this.employeesRepository.findOneByOrFail({ id });
    await this.employeesRepository.update(id, updateEmployeeDto);
    const updatedEmployee = await this.employeesRepository.findOneBy({ id });
    return updatedEmployee;
  }

  async remove(id: number) {
    const removedEmployee = await this.employeesRepository.findOneByOrFail({
      id,
    });
    await this.employeesRepository.remove(removedEmployee);
    return removedEmployee;
  }
}
