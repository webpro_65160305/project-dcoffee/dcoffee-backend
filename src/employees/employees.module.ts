import { Module } from '@nestjs/common';
import { EmployeesService } from './employees.service';
import { EmployeesController } from './employees.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Employee } from './entities/employee.entity';
import { JwtModule } from '@nestjs/jwt'; // Import JwtModule
import { JwtService } from '@nestjs/jwt'; // Import JwtService

@Module({
  imports: [
    TypeOrmModule.forFeature([Employee]),
    JwtModule.register({
      secret: 'your_secret_key', // Provide your secret key here
      signOptions: { expiresIn: '1h' },
    }),
  ],
  controllers: [EmployeesController],
  providers: [EmployeesService, JwtService], // Ensure JwtService is provided
})
export class EmployeesModule {}
