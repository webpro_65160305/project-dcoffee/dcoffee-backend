import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UpdateStockReceiptDto } from './dto/update-StockReceipt.dto';
import { StockReceipt } from './entities/StockReceipt.entity';
import { CreateStockReceiptDto } from './dto/create-StockReceipt.dto';
import { StockReceiptItems } from './entities/StockReceiptItem.entity';
import { Stock } from 'src/Stock/entities/Stock.entity';

@Injectable()
export class StockReceiptService {
  constructor(
    @InjectRepository(StockReceipt)
    private stockReceiptRepository: Repository<StockReceipt>,
    @InjectRepository(StockReceiptItems)
    private stockReceiptItemRepository: Repository<StockReceiptItems>,
    @InjectRepository(Stock) private stockRepository: Repository<Stock>,
  ) {}
  async create(createStockReceiptDto: CreateStockReceiptDto) {
    const stockReceipt = new StockReceipt();
    stockReceipt.totalPrice = 0;
    stockReceipt.totalQuantity = 0;
    stockReceipt.stockReceiptItems = [];
    for (const si of createStockReceiptDto.stockReceiptItems) {
      const stockReceiptItem = new StockReceiptItems();
      stockReceiptItem.stock = await this.stockRepository.findOneBy({
        id: si.stockId,
      });
      stockReceiptItem.name = stockReceiptItem.stock.name;
      stockReceiptItem.price = stockReceiptItem.stock.priceUnit;
      stockReceiptItem.quantity = stockReceiptItem.stock.quantity;
      stockReceiptItem.value = stockReceiptItem.stock.value;
      await this.stockReceiptItemRepository.save(stockReceiptItem);
      stockReceipt.stockReceiptItems.push(stockReceiptItem);
    }
    return this.stockReceiptRepository.save(stockReceipt);
  }
  findAll() {
    return this.stockReceiptRepository.find({
      relations: { stockReceiptItems: true },
      order: {
        id: 'desc',
      },
    });
  }
  findOne(id: number) {
    return this.stockReceiptRepository.findOneOrFail({
      where: { id },
      relations: { stockReceiptItems: true },
    });
  }

  async update(id: number, updateStockReceiptDto: UpdateStockReceiptDto) {
    return updateStockReceiptDto;
  }
  async remove(id: number) {
    const deleteStock = await this.stockReceiptRepository.findOneOrFail({
      where: { id },
    });
    await this.stockReceiptRepository.remove(deleteStock);
    return deleteStock;
  }
}
