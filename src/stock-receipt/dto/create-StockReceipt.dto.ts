export class CreateStockReceiptDto {
  stockReceiptItems: {
    stockId: number;
    quantity: number;
  }[];
  receiptDate: string;
}
