import { Module } from '@nestjs/common';

import { TypeOrmModule } from '@nestjs/typeorm';
import { StockReceipt } from './entities/StockReceipt.entity';
import { StockReceiptItems } from './entities/StockReceiptItem.entity';
import { Stock } from 'src/Stock/entities/Stock.entity';
import { StockReceiptController } from './stock-receipt.controller';
import { StockReceiptService } from './stock-receipt.service';

@Module({
  imports: [TypeOrmModule.forFeature([StockReceipt, StockReceiptItems, Stock])],
  controllers: [StockReceiptController],
  providers: [StockReceiptService],
})
export class StockReceiptModule {}
