import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  UseInterceptors,
  UploadedFile,
} from '@nestjs/common';
import { UpdateStockReceiptDto } from './dto/update-StockReceipt.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { extname } from 'path';
import { uuid } from 'uuidv4';
import { StockReceiptService } from './stock-receipt.service';

@Controller('stockReceipts')
export class StockReceiptController {
  constructor(private readonly stocksService: StockReceiptService) {}

  @Get()
  findAll() {
    return this.stocksService.findAll();
  }
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.stocksService.findOne(+id);
  }

  @Post(':id')
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './public/images/stocks',
        filename: (req, file, cb) => {
          const name = uuid();
          return cb(null, name + extname(file.originalname));
        },
      }),
    }),
  )
  update(
    @Param('id') id: string,
    @Body() updateStockDto: UpdateStockReceiptDto,
    @UploadedFile() file: Express.Multer.File,
  ) {
    console.log(file);
    return this.stocksService.update(+id, updateStockDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.stocksService.remove(+id);
  }

  @Post('upload')
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './public/images/stocks',
        filename: (req, file, cb) => {
          const name = uuid();
          return cb(null, name + extname(file.originalname));
        },
      }),
    }),
  )
  uploadFile(
    @Body() stock: { name: string; age: number },
    @UploadedFile() file: Express.Multer.File,
  ) {
    console.log(stock);
    console.log(file.filename);
    console.log(file.path);
  }
}
