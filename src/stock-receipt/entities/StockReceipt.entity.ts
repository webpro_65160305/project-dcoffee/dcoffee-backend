import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { StockReceiptItems } from './StockReceiptItem.entity';

@Entity()
export class StockReceipt {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  receiptDate: Date;

  @Column()
  totalPrice: number;

  @Column()
  totalQuantity: number;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @OneToMany(
    () => StockReceiptItems,
    (stockReceiptItem) => stockReceiptItem.stockReceipt,
  )
  stockReceiptItems: StockReceiptItems[];
}
