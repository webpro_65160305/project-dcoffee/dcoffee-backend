import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity()
export class Member {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    name: 'name',
    default: '',
  })
  name: string;

  @Column()
  tel: string;

  @Column()
  point: number;
}
