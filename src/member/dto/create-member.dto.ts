export class CreateMemberDto {
  name: string;
  tel: string;
  point: number;
}
