import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateMemberDto } from 'src/member/dto/create-member.dto';
import { UpdateMemberDto } from 'src/member/dto/update-member.dto';
import { Member } from 'src/member/entities/member.entity';
import { Repository } from 'typeorm';

@Injectable()
export class MemberService {
  constructor(
    @InjectRepository(Member)
    private memberRepository: Repository<Member>,
  ) {}
  create(createMemberDto: CreateMemberDto) {
    return this.memberRepository.save(createMemberDto);
  }

  findAll() {
    return this.memberRepository.find();
  }

  findOne(id: number) {
    return this.memberRepository.findOneBy({ id });
  }

  async findByTel(tel: string): Promise<Member | undefined> {
    return this.memberRepository.findOneByOrFail({ tel });
  }

  async update(id: number, updateMemberDto: UpdateMemberDto) {
    await this.memberRepository.findOneByOrFail({ id });
    await this.memberRepository.update(id, updateMemberDto);
    const updatedMember = await this.memberRepository.findOneBy({ id });
    return updatedMember;
  }

  async remove(id: number) {
    const removedMember = await this.memberRepository.findOneByOrFail({
      id,
    });
    await this.memberRepository.remove(removedMember);
    return removedMember;
  }
}
