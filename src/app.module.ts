import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { StockModule } from './Stock/Stock.module';
import { UsersModule } from './users/users.module';
import { RolesModule } from './roles/roles.module';
import { TypesModule } from './types/types.module';
import { ProductsModule } from './products/products.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { OrdersModule } from './orders/orders.module';
import { DataSource } from 'typeorm';
import { User } from './users/entities/user.entity';
import { Role } from './roles/entities/role.entity';
import { Order } from './orders/entities/order.entity';
import { Type } from './types/entities/type.entity';
import { Product } from './products/entities/product.entity';
import { OrderItem } from './orders/entities/orderItem.entity';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';
import { AuthModule } from './auth/auth.module';
import { Stock } from './Stock/entities/Stock.entity';
import { StockReceipt } from './stock-receipt/entities/StockReceipt.entity';
import { StockReceiptItems } from './stock-receipt/entities/StockReceiptItem.entity';
import { StockReceiptModule } from './stock-receipt/stock-receipt.module';
import { EmployeesModule } from './employees/employees.module';
import { Employee } from './employees/entities/employee.entity';
import { MemberModule } from './member/member.module';
import { Member } from './member/entities/member.entity';
import { BranchsModule } from './branchs/branchs.module';
import { Branch } from './branchs/entities/branch.entity';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: 'mydb.sqlite',
      entities: [
        Stock,
        User,
        Role,
        Order,
        OrderItem,
        Type,
        Product,
        StockReceipt,
        StockReceiptItems,
        Employee,
        Member,
        Branch,
      ],
      synchronize: true,
      logging: false,
    }),
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'public'),
    }),
    UsersModule,
    RolesModule,
    OrdersModule,
    TypesModule,
    ProductsModule,
    AuthModule,
    StockReceiptModule,
    StockModule,
    EmployeesModule,
    MemberModule,
    BranchsModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
